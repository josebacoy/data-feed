from neo4j import GraphDatabase
import logging
from neo4j.exceptions import ServiceUnavailable
import configparser
import pyodbc
from azure.storage.blob import BlobClient
import sys

class App:

    def __init__(self, config):
        try:
            uri  = config['neo4j']['uri']
            user = config['neo4j']['user']
            password = config['neo4j']['password']
            self.driver = GraphDatabase.driver(uri, auth=(user, password))
        except Exception as ex:
            logging.error("Unable to create graph database session with error: {ex}".format(ex=ex))
            sys.exit(1)
		
    def close(self):
        self.driver.close()

    def execute_write_to_neo4j(self, cypher_query):
        try:
            with self.driver.session() as session:
                result = session.write_transaction(self._write_to_neo4j, cypher_query)
                print("Write transaction result: {n}".format(n=len(result)))
                return len(result)
        except Exception as ex:
            sys.exit(1)

    @staticmethod
    def _write_to_neo4j(tx, cypher_query):
        result = tx.run(cypher_query)
        try:
            return [{"result": row["n"]} for row in result]
        except ServiceUnavailable as exception:
            logging.error("{cypher_query} raised an error: \n {exception}".format(
                query=cypher_query, exception=exception))
            sys.exit(1)
        except Exception as ex:
            logging.error("Exception on write_to_neo4j with error: {ex}".format(ex=ex))
            sys.exit(1)            
			
    def get_data_entity(self, config, data_type):
        try:
            db_config = config['sql server']['db_config']
            conn = pyodbc.connect(db_config)
            cursor = conn.cursor()
            query = ( 
                " select NEWID() as uuid, data_type, entity_type, sqlquery, cypquery "  
	        " from neo4j_queries where data_type = ? order by sequence; "
                )
            cursor.execute(query, (data_type,))
            rows  = cursor.fetchall()
            cursor.close()
            conn.close()
            return rows

        except pyodbc.Error as ex:
            sqlstate = ex.args[1]
            print('Encountered error on get_data_entity!!!')
            print(sqlstate)
            conn.close()
            sys.exit(1)

    def upload_csv(self, config):  
        try:
            conn_str = config['azure']['conn_str'] 
            file_csv = config['azure']['file_csv']
            cntr_name= config['azure']['cntr_name']
            blob_name= config['azure']['blob_name']

            blob = BlobClient.from_connection_string(conn_str=conn_str, container_name=cntr_name, blob_name=blob_name)
            blob.delete_blob()
            with open(file_csv, "rb") as data:
                blob.upload_blob(data)
        except Exception as ex:
            print('Encountered error on upload_csv: ', ex)
            sys.exit(1)

def get_app_config(filepath):
    try:
        #get configuration info
        config = configparser.ConfigParser()
        config.read(filepath)

        #connect to sql server
        server   = config['sql server']['db_server']
        database = config['sql server']['db_name']
        username = config['sql server']['db_user']
        password = config['sql server']['db_psw']
        db_config = 'DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+password
        config['sql server']['db_config'] = db_config
        return config
    except Exception as ex:
        print('Encountered error on get_app_config', ex)
        sys.exit(1)

if __name__ == "__main__": 
    configfilepath = "/home/SSAdmin/arb/config/app.neo4j.conf"
    config = get_app_config(configfilepath)
    app = App(config)
    app.upload_csv(config)
    rows = app.get_data_entity(config, 'ARB')
    for row in rows:
        uuid, data_type, entity_type, sqlquery, cypquery = row
        print('Processing for: ', data_type, ' ', entity_type)
        rowcount = app.execute_write_to_neo4j(cypquery)
    app.close()
    sys.exit(0)
