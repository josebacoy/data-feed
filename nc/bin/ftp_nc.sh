#!/bin/bash

home="/home/SSAdmin/nc"
tmp=$home/tmp
logs=$home/logs
files=$home/files
config=$home/config

logfile=$logs/`date +"%Y%m%d_%H%M%S"`.log
echo "Start at: `date +%Y%m%d_%H%M%S`" >> $logfile

HOST=$(grep ftp_host $config/app.conf | cut -d '=' -f 2-)
USER=$(grep ftp_user $config/app.conf | cut -d '=' -f 2-)
PASSWD=$(grep ftp_password $config/app.conf | cut -d '=' -f 2-)
FILEDATE=$(grep ftp_filedate $config/app.conf | cut -d '=' -f 2-)
FILEPREFIX=$(grep ftp_fileprefix $config/app.conf | cut -d '=' -f 2-)

DB_SERVER=$(grep db_server $config/app.conf | cut -d '=' -f 2-)
DB_USER=$(grep db_user $config/app.conf | cut -d '=' -f 2-)
DB_PASSWORD=$(grep db_password $config/app.conf | cut -d '=' -f 2-)
DB_DATABASE=$(grep db_database $config/app.conf | cut -d '=' -f 2-)

body=$(grep email_body $config/app.conf | cut -d '=' -f 2-)
subject=$(grep email_subject $config/app.conf | cut -d '=' -f 2-)
to=$(grep email_to $config/app.conf | cut -d '=' -f 2-) 

tmpcsv=$tmp/*.csv
ncfile=${FILEPREFIX}_`TZ=EST date -d "$FILEDATE"  +%Y-%m-%d`.csv
filename=$tmp/$ncfile
tmpout=$tmp/tmp.out
nccsv=$tmp/nc.csv
error=$logs/error.out

sendmailcmd="/usr/sbin/sendmail"
ncftpgetcmd="/usr/bin/ncftpget"
bcpcmd="/opt/mssql-tools/bin/bcp"
sqlcmd="/opt/mssql-tools/bin/sqlcmd"
grepcmd="/bin/grep"
rmcmd="/bin/rm"
mvcmd="/bin/mv"
awkcmd="/usr/bin/awk"
sedcmd="/bin/sed"
csvtoolcmd="/usr/bin/csvtool"

cd $tmp
$ncftpgetcmd -u $USER -p $PASSWD -a -d stdout ftp://$HOST/$ncfile $tmp > $logs/out
sftp_status=$?
if [ $sftp_status != 0 ];then
   body="Encountered error in ncFTP...\n $(tail -5 $logs/out)"
   echo -e "Subject: ${subject}\n${body}" | $sendmailcmd -t "${to}"
   exit 1
fi

for f in $filename
do 
    if [[ "$f" == *$FILEPREFIX* ]];then
        echo "Processing file: $f " >> $logfile
        touch $files/$(basename "$f")
        $mvcmd $f  $tmpout
        $csvtoolcmd -u '|' cat $tmpout > $nccsv
        $rmcmd -f $tmpout
        $bcpcmd dbo.new_constructs_ratings in $nccsv -S $DB_SERVER -d $DB_DATABASE -U $DB_USER -P $DB_PASSWORD -b 5000 -m 999999 -F2 -q -c -t "|" -e $error 2>/dev/null 
        if [ -s $error ];then
             body="Encounter  error during bcp"       
        fi
    fi
done
echo -e "Subject: ${subject}\n${body}" | $sendmailcmd -t "${to}"
echo "End at: `date +%Y%m%d_%H%M%S`" >> $logfile
echo "" >> $logfile

exit 0

