import json 
import requests
import pandas as pd
import time
import sys
import time

epoch = 1577836800
epoch = int(time.time())
apikey = 'ApiKey e5ac96cd-6010-4201-8cb0-661443072caa'
url = 'https://di.apidata.world/news/api/newsfeed/GetNews?entityTypeIds=1&languages=en&categoryIds=13,27&countryIds=1&lastNewsUpdateTime='+str(epoch)
# insert data from dataframe.
data = requests.get(url, headers={"Authorization": apikey})
if not data.ok:
    sys.exit(1)
# convert into pandas and normalize
df = pd.json_normalize(data.json().get('newsItems'))
df.columns = ['newsId', 'clusterIds', 'title', 'description', 'link', 'imageUrl',
       'newsPublishedOn', 'modifiedOn', 'entities', 'categories', 'language',
       'sourceId', 'sourceName', 'sourceUserName']
df["description"]= df["description"].str.replace("|", "/")
df["title"]=df["title"].str.replace("|", "/")
# save as csv file
df.to_csv('/home/SSAdmin/news/tmp/out.csv', sep='|', header=True, index=False)
sys.exit(0)
