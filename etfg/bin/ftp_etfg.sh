#!/bin/bash

home="/home/SSAdmin/etfg"
tmp=$home/tmp
logs=$home/logs
files=$home/files
config=$home/config

HOST=$(grep ftp_host $config/app.conf | cut -d '=' -f 2-)
USER=$(grep ftp_user $config/app.conf | cut -d '=' -f 2-)
PASSWD=$(grep ftp_password $config/app.conf | cut -d '=' -f 2-)
FILEDATE=$(grep ftp_filedate $config/app.conf | cut -d '=' -f 2-)

DB_SERVER=$(grep db_server $config/app.conf | cut -d '=' -f 2-)
DB_NAME=$(grep db_name $config/app.conf | cut -d '=' -f 2-)
DB_USER=$(grep db_user $config/app.conf | cut -d '=' -f 2-)
DB_PSW=$(grep db_psw $config/app.conf | cut -d '=' -f 2-)
DB_DATA_RETENTION=$(grep db_data_retention $config/app.conf | cut -d '=' -f 2-)

subject=$(grep subject $config/app.conf | cut -d '=' -f 2-)
partner_name=$(grep partner $config/app.conf | cut -d '=' -f 2- | awk -F \" '{print $1}')
body=$(grep body $config/app.conf | cut -d '=' -f 2-)
to=$(grep to_mail $config/app.conf | cut -d '=' -f 2-)

tmpcsv=$tmp/*.csv
toolcsv=$tmp/tool.csv
error=$logs/error

sendmailcmd="/usr/sbin/sendmail"
bcp="/opt/mssql-tools/bin/bcp"
sqlcmd="/opt/mssql-tools/bin/sqlcmd"
grepcmd="/bin/grep"
rmcmd="/bin/rm"
csvtoolcmd="/usr/bin/csvtool"

logfile=$logs/`date +"%Y%m%d_%H%M%S"`.log
echo "Start at: `date +%Y%m%d_%H%M%S`" >> $logfile

cd $tmp

lftp<<END_SCRIPT
open sftp://$HOST
user $USER $PASSWD
get ./constituents_us/ytd/`TZ=EST date -d "$FILEDATE"  +%Y%m%d`_constituents_v2.csv
get ./analytics_us/ytd/`TZ=EST date -d "$FILEDATE"  +%Y%m%d`_analytics_v2.csv
get ./fundflow_us/ytd/`TZ=EST date -d "$FILEDATE"  +%Y%m%d`_fundflow_v2.csv
get ./industry_us/ytd/`TZ=EST date -d "$FILEDATE"  +%Y%m%d`_industries_v2.csv
bye
END_SCRIPT

sftp_status=$?
if [ $sftp_status != 0 ];then
   echo "Encountered error in SFTP for ETFG..."
   echo -e "Subject: ${subject}\n${body}" | $sendmailcmd -t "${to}"
   exit 1
fi

for f in $tmpcsv
do
  $csvtoolcmd -u '|' cat $f > $toolcsv
  if [[ "$f" == *"constituents"* ]];then 
     echo "Processing $f file..." >> $logfile
     sqltext="delete from dbo.etfg_constituents where etfg_date < '$(TZ=EST date -d "$DB_DATA_RETENTION" +%Y-%m-%d)';"
     $sqlcmd -S $DB_SERVER -d $DB_NAME -U $DB_USER -P $DB_PSW -I -Q "$sqltext" > $logs/tmp_constituents.log
     cat  $logs/tmp_constituents.log >> $logfile
     $bcp dbo.etfg_constituents in $toolcsv -S $DB_SERVER -d $DB_NAME -U $DB_USER -P $DB_PSW -b 5000 -m 999999 -q -c -t "|" -e $error >> $logs/tmp_constituents.log
     $grepcmd "rows copied." $logs/tmp_constituents.log >> $logfile
     if [ -s $error ];then
          body="Encounter error during bcp etfg_constituents on file: ${f}"
          echo -e "Subject: ${subject}\n${body}" | $sendmailcmd -t "${to}"
     fi

  fi
  if [[ "$f" == *"industries"* ]];then
     echo "Processing $f file..." >> $logfile
     sqltext="delete from dbo.etfg_industries where etfg_date < '$(TZ=EST date -d "$DB_DATA_RETENTION" +%Y-%m-%d)';"
     $sqlcmd -S $DB_SERVER -d $DB_NAME -U $DB_USER -P $DB_PSW -I -Q "$sqltext" > $logs/tmp_industries.log
     cat  $logs/tmp_industries.log >> $logfile
     $bcp dbo.etfg_industries in $toolcsv -S $DB_SERVER -d $DB_NAME -U $DB_USER -P $DB_PSW -b 1000 -m 999999 -q -c -t "|" -e $error >> $logs/tmp_industries.log
     $grepcmd "rows copied." $logs/tmp_industries.log >> $logfile
     if [ -s $error ];then
          body="Encounter error during bcp etfg_industries on file: ${f}"
          echo -e "Subject: ${subject}\n${body}" | $sendmailcmd -t "${to}"
     fi

  fi
  if [[ "$f" == *"fundflow"* ]];then
     echo "Processing $f file..." >> $logfile
     sqltext="delete from dbo.etfg_fundflow where etfg_date < '$(TZ=EST date -d "$DB_DATA_RETENTION" +%Y-%m-%d)';"
     $sqlcmd -S $DB_SERVER -d $DB_NAME -U $DB_USER -P $DB_PSW -I -Q "$sqltext" > $logs/tmp_fundflow.log
     cat  $logs/tmp_fundflow.log >> $logfile
     $bcp dbo.etfg_fundflow in $toolcsv -S $DB_SERVER -d $DB_NAME -U $DB_USER -P $DB_PSW -b 1000 -m 999999 -q -c -t "|" -e $error  >> $logs/tmp_fundflow.log
     $grepcmd "rows copied." $logs/tmp_fundflow.log  >> $logfile
     if [ -s $error ];then
          body="Encounter error during bcp etfg_fundflow on file: ${f}"
          echo -e "Subject: ${subject}\n${body}" | $sendmailcmd -t "${to}"
     fi

  fi
  if [[ "$f" == *"analytics"* ]];then
     echo "Processing $f file..." >> $logfile
     sqltext="delete from dbo.etfg_analytics where etfg_date < '$(TZ=EST date -d "$DB_DATA_RETENTION" +%Y-%m-%d)';"
     $sqlcmd -S $DB_SERVER -d $DB_NAME -U $DB_USER -P $DB_PSW -I -Q "$sqltext" > $logs/tmp_analytics.log
     cat  $logs/tmp_analytics.log >> $logfile
     $bcp dbo.etfg_analytics in $toolcsv -S $DB_SERVER -d $DB_NAME -U $DB_USER -P $DB_PSW -b 1000 -m 999999 -q -c -t "|" -e $error  >> $logs/tmp_analytics.log
     $grepcmd "rows copied." $logs/tmp_analytics.log >> $logfile
     if [ -s $error ];then
          body="Encounter error during bcp etfg_analytics on file: ${f}"
          echo -e "Subject: ${subject}\n${body}" | $sendmailcmd -t "${to}"
     fi

  fi
  touch $files/$(basename "$f")
done

body="${partner_name} Data Feed is completed"
subject="${partner_name} Notification (Please do not reply to this email)"
echo -e "Subject: ${subject}\n${body}" | $sendmailcmd -t "${to}"

echo "End at: `date +%Y%m%d_%H%M%S`" >> $logfile
echo "" >> $logfile

$rmcmd $tmpcsv
exit 0

