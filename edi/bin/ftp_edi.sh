#!/bin/bash

home="/home/SSAdmin/edi"
tmp=$home/tmp
logs=$home/logs
files=$home/files
config=$home/config

rm -f $tmp/*
rm -f $tmp/Adjusted/*
rm -f $tmp/Fund/*.zip
rm -f $tmp/Fund/files/*
rm -f $tmp/Prices/*.zip
rm -f $tmp/Prices/files/*

logfile=$logs/`date +"%Y%m%d_%H%M%S"`.log
echo "Start at: `date +%Y%m%d_%H%M%S`" >> $logfile

HOST=$(grep ftp_host $config/app.conf | cut -d '=' -f 2-)
BKUP_HOST=$(grep ftp_bkup_host $config/app.conf | cut -d '=' -f 2-)
USER=$(grep ftp_user $config/app.conf | cut -d '=' -f 2-)
PASSWD=$(grep ftp_password $config/app.conf | cut -d '=' -f 2-)
FILEDATE=$(grep ftp_filedate $config/app.conf | cut -d '=' -f 2-)
FILEPREFIX=$(grep ftp_fileprefix $config/app.conf | cut -d '=' -f 2-)

DB_SERVER=$(grep db_server $config/app.conf | cut -d '=' -f 2-)
DB_USER=$(grep db_user $config/app.conf | cut -d '=' -f 2-)
DB_PASSWORD=$(grep db_password $config/app.conf | cut -d '=' -f 2-)
DB_DATABASE=$(grep db_database $config/app.conf | cut -d '=' -f 2-)

body=$(grep email_body $config/app.conf | cut -d '=' -f 2-)
subject=$(grep email_subject $config/app.conf | cut -d '=' -f 2-)
to=$(grep email_to $config/app.conf | cut -d '=' -f 2-) 

tmpcsv=$tmp/*.csv
edifile=`TZ=EST date -d "$FILEDATE"  +%Y%m%d`_*.txt
edizip=*`TZ=EST date -d "$FILEDATE"  +%Y%m%d`.zip
edifund=US_*`TZ=EST date -d "$FILEDATE"  +%y%m%d`.zip
editxt=*`TZ=EST date -d "$FILEDATE"  +%y%m%d`*.txt
filename=$tmp/$edifile
tmpout=$tmp/tmp.out
edicsv=$tmp/edi.txt
error=$logs/error.out
tmpin=$tmp/Prices/files/tmp.in

sendmailcmd="/usr/sbin/sendmail"
ncftpgetcmd="/usr/bin/ncftpget"
bcpcmd="/opt/mssql-tools/bin/bcp"
sqlcmd="/opt/mssql-tools/bin/sqlcmd"
grepcmd="/bin/grep"
rmcmd="/bin/rm"
mvcmd="/bin/mv"
awkcmd="/usr/bin/awk"
sedcmd="/bin/sed"
csvtoolcmd="/usr/bin/csvtool"
catcmd="/bin/cat"
unzipcmd="/usr/bin/unzip"
prices_list=$tmp/Prices/prices_list.txt
countries=$tmp/Prices/countries.txt

cd $tmp
lftp<<END_SCRIPT
open sftp://$HOST
user $USER $PASSWD
mget /Bespoke/StockSmart/$edifile
bye
END_SCRIPT

sftp_status=$?
if [ $sftp_status != 0 ];then

   lftp<<END_SCRIPT
open sftp://$BKUP_HOST
user $USER $PASSWD
mget /Bespoke/StockSmart/$edifile
bye
END_SCRIPT

   sftp_statusbk=$?
   if [ $sftp_statusbk != 0 ];then
      body="Encountered error in FTP get edifile..."
      echo -e "Subject: ${subject}\n${body}" | $sendmailcmd -t "${to}"
   fi
fi

for f in $filename
do 
    if [[ "$f" == *695* ]];then
        echo "Processing file: $f " >> $logfile
        touch $files/$(basename "$f")
        $catcmd $f | head -n -1 > $edicsv 
        $bcpcmd dbo.edi_event_695 in $edicsv -S $DB_SERVER -d $DB_DATABASE -U $DB_USER -P $DB_PASSWORD -m 999999 -F2 -q -c -e $error 2>/dev/null 
        if [ -s $error ];then
             body="Encounter  error during bcp 695 on file: ${f}"       
        fi
        #echo -e "Subject: ${subject}\n${body}" | $sendmailcmd -t "${to}"
    fi

    if [[ "$f" == *689* ]];then
        echo "Processing file: $f " >> $logfile
        touch $files/$(basename "$f")
        $catcmd $f | head -n -1 > $edicsv
        $bcpcmd dbo.edi_event_689 in $edicsv -S $DB_SERVER -d $DB_DATABASE -U $DB_USER -P $DB_PASSWORD -m 999999 -F2 -q -c -e $error 2>/dev/null
        if [ -s $error ];then
             body="Encounter  error during bcp 689 on file: ${f}"
        fi
        #echo -e "Subject: ${subject}\n${body}" | $sendmailcmd -t "${to}"
    fi 

    if [[ "$f" == *698* ]];then
        echo "Processing file: $f " >> $logfile
        touch $files/$(basename "$f")
        $catcmd $f | head -n -1 > $edicsv
        $bcpcmd dbo.edi_event_698 in $edicsv -S $DB_SERVER -d $DB_DATABASE -U $DB_USER -P $DB_PASSWORD -m 999999 -F2 -q -c -e $error 2>/dev/null
        if [ -s $error ];then
             body="Encounter  error during bcp 698 on file: ${f}"
        fi
        #echo -e "Subject: ${subject}\n${body}" | $sendmailcmd -t "${to}"
    fi

    if [[ "$f" == *SRF* ]];then
        echo "Processing file: $f " >> $logfile
        touch $files/$(basename "$f")
        $catcmd $f | head -n -1 > $edicsv
        $bcpcmd dbo.edi_event_srf in $edicsv -S $DB_SERVER -d $DB_DATABASE -U $DB_USER -P $DB_PASSWORD -m 999999 -F2 -q -c -e $error 2>/dev/null
        if [ -s $error ];then
             body="Encounter  error during bcp SRF on file: ${f}"
        fi
        #echo -e "Subject: ${subject}\n${body}" | $sendmailcmd -t "${to}"
    fi

done

cd $tmp/Fund
lftp<<END_SCRIPT
open sftp://$HOST
user $USER $PASSWD
mget /Prices/P04/US/Fund/$edifund
bye
END_SCRIPT

sftp_status=$?
if [ $sftp_status != 0 ];then

   lftp<<END_SCRIPT
open sftp://$BKUP_HOST
user $USER $PASSWD
mget /Prices/P04/US/Fund/$edifund
bye
END_SCRIPT

   sftp_statusbk=$?
   if [ $sftp_statusbk != 0 ];then
      body="Encountered error in FTP Prices Fund..."
      echo -e "Subject: ${subject}\n${body}" | $sendmailcmd -t "${to}"
   fi
fi

for f in $tmp/Fund/*
do
    if [[ "$f" == *US* ]];then
        echo "Processing file: $f " >> $logfile
        touch $files/$(basename "$f")
        rm -f $tmp/Fund/files/*
        $unzipcmd $f -d $tmp/Fund/files
        $catcmd $tmp/Fund/files/*.TXT | head -n -1 > $tmp/Fund/files/tmp.in
        $bcpcmd dbo.edi_price_fund in $tmp/Fund/files/tmp.in -S $DB_SERVER -d $DB_DATABASE -U $DB_USER -P $DB_PASSWORD -b 5000 -m 999999 -F2 -q -c -e $error 2>/dev/null
        if [ -s $error ];then
             body="Encounter  error during bcp price fund on file: ${f}"
             echo -e "Subject: ${subject}\n${body}" | $sendmailcmd -t "${to}"
        fi
    fi
done

cd $tmp/Adjusted
lftp<<END_SCRIPT > $prices_list
open sftp://$HOST
user $USER $PASSWD
ls -l /Prices/P04/
bye
END_SCRIPT

sftp_statusbk=$?
if [ $sftp_statusbk != 0 ];then
      body="Encountered error in FTP get prices adjustments country list..."
      echo -e "Subject: ${subject}\n${body}" | $sendmailcmd -t "${to}"
      exit 1
fi

$awkcmd '{print $NF}' $prices_list > $countries
while read line; do
  echo "Processing country code: $line" >> $logfile
  $rmcmd -f $tmp/Adjusted/*
  lftp<<END_SCRIPT
  open sftp://$HOST
  user $USER $PASSWD
  mget /Prices/P04/$line/Adjusted/$line$editxt
  bye
END_SCRIPT

sftp_status=$?
if [ $sftp_statusbk != 0 ];then
  $rmcmd -f $tmp/Adjusted/*
  lftp<<END_SCRIPT
  open sftp://$BKUP_HOST
  user $USER $PASSWD
  mget /Prices/P04/$line/Adjusted/$line$editxt
  bye
END_SCRIPT
fi

  sftp_statusbk=$? 
  if [ $sftp_statusbk != 0 ];then
      body="Encountered error in FTP get Prices Adjustments for country: $line"
      echo -e "Subject: ${subject}\n${body}" | $sendmailcmd -t "${to}"
  fi

  for f in $tmp/Adjusted/*
  do
    if [[ "$f" == *.txt* ]];then
        echo "Processing file $f for country code: $line" >> $logfile
        touch $files/$(basename "$f")
        if [ `cat $f | wc -l` -ge "2" ];then
           $bcpcmd dbo.edi_price_adjusted in $f -S $DB_SERVER -d $DB_DATABASE -U $DB_USER -P $DB_PASSWORD -b 5000 -m 999999 -F2 -q -c -e $error 2>/dev/null
           if [ -s $error ];then
             body="Encounter  error during bcp prices adjustments on file: ${f}\n $(head -5 $error)"
             echo -e "Subject: ${subject}\n${body}" | $sendmailcmd -t "${to}"
           fi
        fi
    fi
  done
done < $countries

cd $tmp/Prices
lftp<<END_SCRIPT > $prices_list
open sftp://$HOST
user $USER $PASSWD
ls -l /Prices/P04
bye
END_SCRIPT

sftp_statusbk=$?
if [ $sftp_statusbk != 0 ];then
      body="Encountered error in FTP get prices country list..."
      echo -e "Subject: ${subject}\n${body}" | $sendmailcmd -t "${to}"
      exit 1
fi

awk '{print $NF}' $prices_list > $countries

while read line; do
  echo "Processing country code: $line" >> $logfile
  $rmcmd -f $tmp/Prices/*.zip
  lftp<<END_SCRIPT
  open sftp://$HOST
  user $USER $PASSWD
  mget /Prices/P04/$line/$line$edizip
  bye
END_SCRIPT

sftp_statusbk=$? 
if [ $sftp_statusbk != 0 ];then
      body="Encountered error in FTP get Prices for country: $line"
      echo -e "Subject: ${subject}\n${body}" | $sendmailcmd -t "${to}"
fi

  for f in $tmp/Prices/*
  do
    if [[ "$f" == *.zip* ]];then
        echo "Processing file: $f " >> $logfile
        touch $files/$(basename "$f")
        $rmcmd -rf $tmp/Prices/files/*
        $unzipcmd $f -d $tmp/Prices/files
        $catcmd $tmp/Prices/files/*.TXT | head -n -1 > $tmpin
        if [ -s $tmpin ];then
           $bcpcmd dbo.edi_prices in $tmpin -S $DB_SERVER -d $DB_DATABASE -U $DB_USER -P $DB_PASSWORD -b 5000 -m 999999 -F2 -q -c -e $error 2>/dev/null
           if [ -s $error ];then
             body="Encounter  error during bcp prices on file: ${f}\n $(head -5 $error)"
             echo -e "Subject: ${subject}\n${body}" | $sendmailcmd -t "${to}"
           fi
        fi
    fi
  done

done < $countries

$rmcmd -f $tmp/Xrates/*.txt
awkcmd="/usr/bin/awk"

xrates_list=$tmp/Xrates/xrates_list
xrates=$tmp/Xrates/xrates
xratetxt=`TZ=EST date -d "$FILEDATE"  +%Y-%m-%d`_*.txt
tmprates=$tmp/Xrates/tmprates

cd $tmp/Xrates
lftp<<END_SCRIPT > $xrates_list
open sftp://$HOST
user $USER $PASSWD
ls -l /xrates/01
bye
END_SCRIPT

sftp_statusbk=$?
if [ $sftp_statusbk != 0 ];then
      body="Encountered error in FTP get Xrates list..."
      echo -e "Subject: ${subject}\n${body}" | $sendmailcmd -t "${to}"
fi

$awkcmd '{print $NF}' $xrates_list > $xrates

while read line; do
  echo "Processing xrates country code: $line" >> $logfile
  $rmcmd -f $tmp/Xrates/*.txt
  lftp<<END_SCRIPT
  open sftp://$HOST
  user $USER $PASSWD
  mget /xrates/01/$line/$xratetxt
  bye
END_SCRIPT

sftp_statusbk=$? 
if [ $sftp_statusbk != 0 ];then
      body="Encountered error in FTP get Xchange rates for country: $line"
      echo -e "Subject: ${subject}\n${body}" | $sendmailcmd -t "${to}"
fi

  for f in $tmp/Xrates/*
  do
    if [[ "$f" == *.txt* ]];then
        echo "Processing file: $f " >> $logfile
        touch $files/$(basename "$f")
        $catcmd $f | head -n -1 > $tmprates
        if [ -s $tmprates ];then
           $bcpcmd dbo.edi_daily_rates in $tmprates -S $DB_SERVER -d $DB_DATABASE -U $DB_USER -P $DB_PASSWORD -b 5000 -m 999999 -c -F2 -q -e $error 2>/dev/null
           if [ -s $error ];then
             body="Encounter  error during bcp edi daily rates on file: ${f}\n $(head -5 $error)"
             echo -e "Subject: ${subject}\n${body}" | $sendmailcmd -t "${to}"
           fi
        fi
    fi
  done
done < $xrates

echo "End at: `date +%Y%m%d_%H%M%S`" >> $logfile
body="EDI Exchange Data Feed is success\n $(tail $error)"
echo -e "Subject: ${subject}\n${body}" | $sendmailcmd -t "${to}"

echo "" >> $logfile

exit 0

