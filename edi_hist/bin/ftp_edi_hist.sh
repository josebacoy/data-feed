#!/bin/bash

home="/home/SSAdmin/edi/edi_hist"
tmp=$home/tmp
logs=$home/logs
files=$home/files
config=$home/config

logfile=$logs/`date +"%Y%m%d_%H%M%S"`.log
echo "Start at: `date +%Y%m%d_%H%M%S`" >> $logfile

HOST=$(grep ftp_host $config/app.conf | cut -d '=' -f 2-)
BKUP_HOST=$(grep ftp_bkup_host $config/app.conf | cut -d '=' -f 2-)
USER=$(grep ftp_user $config/app.conf | cut -d '=' -f 2-)
PASSWD=$(grep ftp_password $config/app.conf | cut -d '=' -f 2-)
FILEDATE=$(grep ftp_filedate $config/app.conf | cut -d '=' -f 2-)
FILEPREFIX=$(grep ftp_fileprefix $config/app.conf | cut -d '=' -f 2-)

DB_SERVER=$(grep db_server $config/app.conf | cut -d '=' -f 2-)
DB_USER=$(grep db_user $config/app.conf | cut -d '=' -f 2-)
DB_PASSWORD=$(grep db_password $config/app.conf | cut -d '=' -f 2-)
DB_DATABASE=$(grep db_database $config/app.conf | cut -d '=' -f 2-)

body=$(grep email_body $config/app.conf | cut -d '=' -f 2-)
subject=$(grep email_subject $config/app.conf | cut -d '=' -f 2-)
to=$(grep email_to $config/app.conf | cut -d '=' -f 2-) 

tmpcsv=$tmp/*.csv
edifile=`TZ=EST date -d "$FILEDATE"  +%Y%m%d`_*.txt
edizip=US_*`TZ=EST date -d "$FILEDATE"  +%Y%m%d`.zip
edifund=US_*`TZ=EST date -d "$FILEDATE"  +%y%m%d`.zip
editxt=US_*`TZ=EST date -d "$FILEDATE"  +%y%m%d`.txt
filename_689=$tmp/689/*
filename_695=$tmp/695/*
filename_SRF=$tmp/698/*
tmpout=$tmp/tmp.out
edicsv=$tmp/edi.txt
error=$logs/error.out

sendmailcmd="/usr/sbin/sendmail"
ncftpgetcmd="/usr/bin/ncftpget"
bcpcmd="/opt/mssql-tools/bin/bcp"
sqlcmd="/opt/mssql-tools/bin/sqlcmd"
grepcmd="/bin/grep"
rmcmd="/bin/rm"
mvcmd="/bin/mv"
awkcmd="/usr/bin/awk"
sedcmd="/bin/sed"
csvtoolcmd="/usr/bin/csvtool"
catcmd="/bin/cat"
unzipcmd="/usr/bin/unzip"

logfile=$logs/`date +"%Y%m%d_%H%M%S"`.log
echo "Start at: `date +%Y%m%d_%H%M%S`" >> $logfile

for f in $tmp/Prices/*
do
    if [[ "$f" == *US* ]];then
        echo "Processing file: $f " >> $logfile
        touch $files/$(basename "$f")
        rm -f $tmp/Prices/files/*
        $unzipcmd $f -d $tmp/Prices/files 2>/dev/null
        $catcmd $tmp/Prices/files/*.TXT | head -n -1 > $tmp/Prices/files/tmp.in
        $bcpcmd dbo.edi_prices_hist in $tmp/Prices/files/tmp.in -S $DB_SERVER -d $DB_DATABASE -U $DB_USER -P $DB_PASSWORD -b 5000 -m 999999 -F2 -q -c -e $error >> $logfile
        if [ -s $error ];then
             body="Encounter  error during bcp prices on file: ${f}"
             echo -e "Subject: ${subject}\n${body}" | $sendmailcmd -t "${to}"
        fi
    fi

done

echo -e "Subject: ${subject}\n${body}" | $sendmailcmd -t "${to}"


echo "End at: `date +%Y%m%d_%H%M%S`" >> $logfile
echo "" >> $logfile

exit 0

