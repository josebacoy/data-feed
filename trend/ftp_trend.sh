#!/bin/bash

home="/home/SSAdmin/trend"
tmp=$home/tmp
logs=$home/logs
files=$home/files
config=$home/config

HOST=$(grep ftp_host $config/app.conf | cut -d '=' -f 2-)
USER=$(grep ftp_user $config/app.conf | cut -d '=' -f 2-)
PASSWD=$(grep ftp_password $config/app.conf | cut -d '=' -f 2-)
FILEDATE=$(grep ftp_filedate $config/app.conf | cut -d '=' -f 2-)

DB_SERVER=$(grep db_server $config/app.conf | cut -d '=' -f 2-)
DB_NAME=$(grep db_name $config/app.conf | cut -d '=' -f 2-)
DB_USER=$(grep db_user $config/app.conf | cut -d '=' -f 2-)
DB_PSW=$(grep db_psw $config/app.conf | cut -d '=' -f 2-)
DB_DATA_RETENTION=$(grep db_data_retention $config/app.conf | cut -d '=' -f 2-)

subject=$(grep subject $config/app.conf | cut -d '=' -f 2-)
partner_name=$(grep partner $config/app.conf | cut -d '=' -f 2- | awk -F \" '{print $1}')
body=$(grep body $config/app.conf | cut -d '=' -f 2-)
to=$(grep to_mail $config/app.conf | cut -d '=' -f 2-)

tmpcsv=$tmp/*.csv
toolcsv=$tmp/tool.csv

sendmailcmd="/usr/sbin/sendmail"
bcp="/opt/mssql-tools/bin/bcp"
sqlcmd="/opt/mssql-tools/bin/sqlcmd"
grepcmd="/bin/grep"
rmcmd="/bin/rm"
csvtoolcmd="/usr/bin/csvtool"
unzipcmd="/usr/bin/unzip"
lftpcmd="/usr/bin/lftp"

logfile=$logs/`date +"%Y%m%d_%H%M%S"`.log
bcplog=$logs/trendrating_us.log
ftplog=$logs/ftp.log
error=$logs/bcp.error
echo "Start at: `date +%Y%m%d_%H%M%S`" >> $logfile
csvfile=trendrating-us_`TZ=EST date -d "$FILEDATE"  +%Y%m%d`*.csv

cd $tmp
$lftpcmd -u ${USER},${PASSWD} sftp://${HOST}<<EOF
mget ./us/updates/$csvfile
bye
EOF

sftp_status=$?
if [ $sftp_status != 0 ];then
   body="Encountered error in ftp: ${csvfile}"
   echo -e "Subject: ${subject}\n${body}" | $sendmailcmd -t "${to}"
   exit 1
fi

for f in $tmpcsv
do
  $csvtoolcmd -u '|' cat $f > $toolcsv
  if [[ "$f" == *"csv"* ]];then 
     echo "Processing $f file..." >> $logfile
     $bcp dbo.trendrating_us in $toolcsv -S $DB_SERVER -d $DB_NAME -U $DB_USER -P $DB_PSW -b 5000 -m 999999 -F2 -q -c -t "|" -e $error > $bcplog
     $grepcmd "rows copied." $bcplog >> $logfile
     if [ -s $error ];then
         body="Encounter  error during bcp trendrating on file: ${f}\n $(head -5 $error)" 
         echo -e "Subject: ${subject}\n${body}" | $sendmailcmd -t "${to}"
     fi

  fi
  touch $files/$(basename "$f")
done

body="${partner_name} Data Feed is success"
subject="${partner_name} Notification (Please do not reply to this email)"
echo -e "Subject: ${subject}\n${body}" | $sendmailcmd -t "${to}"

echo "End at: `date +%Y%m%d_%H%M%S`" >> $logfile
echo "" >> $logfile

$rmcmd $tmpcsv
exit 0

