#!/bin/bash

home="/home/SSAdmin/price_adjust"

tmp=$home/tmp
logs=$home/logs
files=$home/files
config=$home/config

logfile=$logs/`date +"%Y%m%d_%H%M%S"`.log
echo "Start at: `date +%Y%m%d_%H%M%S`" >> $logfile

HOST=$(grep ftp_host $config/app.conf | cut -d '=' -f 2-)
USER=$(grep ftp_user $config/app.conf | cut -d '=' -f 2-)
PASSWD=$(grep ftp_password $config/app.conf | cut -d '=' -f 2-)
FILEDATE=$(grep ftp_filedate $config/app.conf | cut -d '=' -f 2-)

DB_SERVER=$(grep db_server $config/app.conf | cut -d '=' -f 2-)
DB_USER=$(grep db_user $config/app.conf | cut -d '=' -f 2-)
DB_PASSWORD=$(grep db_password $config/app.conf | cut -d '=' -f 2-)
DB_DATABASE=$(grep db_database $config/app.conf | cut -d '=' -f 2-)

body=$(grep email_body $config/app.conf | cut -d '=' -f 2-)
subject=$(grep email_subject $config/app.conf | cut -d '=' -f 2-)
to=$(grep email_to $config/app.conf | cut -d '=' -f 2-) 

tmpcsv=$tmp/*.csv
edizip=*`TZ=EST date -d "$FILEDATE"  +%Y%m%d`.zip
editxt=*`TZ=EST date -d "$FILEDATE"  +%y%m%d`*.txt

filename=$tmp/$edifile
tmpout=$tmp/tmp.out
edicsv=$tmp/edi.txt
error=$logs/error.out
tmpin=$tmp/tmp.in

sendmailcmd="/usr/sbin/sendmail"
ncftpgetcmd="/usr/bin/ncftpget"
bcpcmd="/opt/mssql-tools/bin/bcp"
sqlcmd="/opt/mssql-tools/bin/sqlcmd"
grepcmd="/bin/grep"
rmcmd="/bin/rm"
mvcmd="/bin/mv"
awkcmd="/usr/bin/awk"
sedcmd="/bin/sed"
csvtoolcmd="/usr/bin/csvtool"
catcmd="/bin/cat"
unzipcmd="/usr/bin/unzip"
awkcmd="/usr/bin/awk"

$rmcmd -f $tmp/*
$rmcmd -f $tmp/Adjusted/*

prices_list=$tmp/prices_list.txt
countries=$tmp/countries.txt

cd $tmp/Adjusted
lftp<<END_SCRIPT > $prices_list
open sftp://$HOST
user $USER $PASSWD
ls -l /Prices/P04_History/
bye
END_SCRIPT

sftp_statusbk=$?
if [ $sftp_statusbk != 0 ];then
      body="Encountered error in FTP get prices adjustments country list..."
      echo -e "Subject: ${subject}\n${body}" | $sendmailcmd -t "${to}"
fi

$awkcmd '{print $NF}' $prices_list > $countries
while read line; do
  echo "Processing country code: $line" >> $logfile
  $rmcmd -f $tmp/Adjusted/*
  lftp<<END_SCRIPT
  open sftp://$HOST
  user $USER $PASSWD
  mget /Prices/P04_History/$line/Adjusted/$editxt
  bye
END_SCRIPT

sftp_statusbk=$? 
if [ $sftp_statusbk != 0 ];then
      body="Encountered error in FTP get Prices Adjustments for country: $line"
      echo -e "Subject: ${subject}\n${body}" | $sendmailcmd -t "${to}"
fi
  for f in $tmp/Adjusted/*
  do
    if [[ "$f" == *.* ]];then
        echo "Processing file $f for country code: $line" >> $logfile
        touch $files/$(basename "$f")
        if [ `cat $f | wc -l` -ge "2" ];then 
           $bcpcmd dbo.edi_prices_adjustments in $f -S $DB_SERVER -d $DB_DATABASE -U $DB_USER -P $DB_PASSWORD -b 5000 -m 999999 -F2 -q -c -e $error 2>/dev/null
           if [ -s $error ];then
             body="Encounter  error during bcp prices adjustments on file: ${f}\n $(head -5 $error)"
             echo -e "Subject: ${subject}\n${body}" | $sendmailcmd -t "${to}"
           fi
        fi
    fi
  done

done < $countries

echo "End at: `date +%Y%m%d_%H%M%S`" >> $logfile
body="EDI Price Adjustments Data Feed is success\n $(tail $error)"
echo -e "Subject: ${subject}\n${body}" | $sendmailcmd -t "${to}"

echo "" >> $logfile

exit 0

