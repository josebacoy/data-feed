#!/bin/bash

home="/home/SSAdmin/arb"
tmp=$home/tmp
logs=$home/logs
files=$home/files
config=$home/config

HOST=$(grep ftp_host $config/app.conf | cut -d '=' -f 2-)
USER=$(grep ftp_user $config/app.conf | cut -d '=' -f 2-)
PASSWD=$(grep ftp_password $config/app.conf | cut -d '=' -f 2-)
FILEDATE=$(grep ftp_filedate $config/app.conf | cut -d '=' -f 2-)

DB_SERVER=$(grep db_server $config/app.conf | cut -d '=' -f 2-)
DB_NAME=$(grep db_name $config/app.conf | cut -d '=' -f 2-)
DB_USER=$(grep db_user $config/app.conf | cut -d '=' -f 2-)
DB_PSW=$(grep db_psw $config/app.conf | cut -d '=' -f 2-)
DB_DATA_RETENTION=$(grep db_data_retention $config/app.conf | cut -d '=' -f 2-)

subject=$(grep subject $config/app.conf | cut -d '=' -f 2-)
partner_name=$(grep partner $config/app.conf | cut -d '=' -f 2- | awk -F \" '{print $1}')
body=$(grep body $config/app.conf | cut -d '=' -f 2-)
to=$(grep to_mail $config/app.conf | cut -d '=' -f 2-)

tmpcsv=$tmp/*.csv
toolcsv=$tmp/tool.csv
importfile=$tmp/import_arb_sray_262.csv
error=$logs/error

sendmailcmd="/usr/sbin/sendmail"
bcp="/opt/mssql-tools/bin/bcp"
sqlcmd="/opt/mssql-tools/bin/sqlcmd"
grepcmd="/bin/grep"
rmcmd="/bin/rm"
csvtoolcmd="/usr/bin/csvtool"
catcmd="/bin/cat"
awkcmd="/usr/bin/awk"
pythoncmd="/usr/local/bin/python3.6"

logfile=$logs/`date +"%Y%m%d_%H%M%S"`.log
neo4j_error=$logs/neo4j_`date +"%Y%m%d_%H%M%S"`.log
echo "Start at: `date +%Y%m%d_%H%M%S`" >> $logfile

$rmcmd $tmpcsv
cd $tmp

ftp -p -n $HOST <<END_SCRIPT
quote USER $USER
quote PASS $PASSWD
get sray_262_update_`TZ=EST date -d "$FILEDATE"  +%Y%m%d`.csv
get sray_features_262_update_`TZ=EST date -d "$FILEDATE"  +%Y%m%d`.csv
get sray_preferences_300_update_`TZ=EST date -d "$FILEDATE"  +%Y%m%d`.csv
quit
END_SCRIPT

sftp_status=$?
if [ $sftp_status != 0 ];then
   body="Encountered error in ${partner_name} SFTP..."
   echo -e "Subject: ${subject}\n${body}" | $sendmailcmd -t "${to}"
fi

for f in $tmpcsv
do
  $csvtoolcmd -u '|' cat $f > $toolcsv
  if [[ "$f" == *"sray_262_update"* ]];then 
     echo "Processing $f file..." >> $logfile
     #check if schema has changed
     sqlheadtxt="select string_agg(column_name, ',') from information_schema.columns where table_name = 'arb_sray_262';"
     $sqlcmd -S $DB_SERVER -d $DB_NAME -U $DB_USER -P $DB_PSW -I -Q "$sqlheadtxt" > $tmp/current.txt
     sed -n 3p $tmp/current.txt | tr -d " " > $tmp/current_header.txt
     head -1  sray_262_update_`TZ=EST date -d "$FILEDATE"  +%Y%m%d`.csv > $tmp/header.txt
     result=$(diff -y -W 72 $tmp/current_header.txt $tmp/header.txt)
     if [ $? -ne 0 ];then
        echo "Schema has changed" >> $logfile
        body="Schema miscompare error arb_sray_262: ${partner_name}"
        echo -e "Subject: ${subject}\n${body}" | $sendmailcmd -t "${to}"
     fi

     sqltext="delete from dbo.arb_sray_262 where date < '$(TZ=EST date -d "$DB_DATA_RETENTION" +%Y-%m-%d)';"
     $sqlcmd -S $DB_SERVER -d $DB_NAME -U $DB_USER -P $DB_PSW -I -Q "$sqltext" > $logs/tmp_sray_262_update.log
     $catcmd  $logs/tmp_sray_262_update.log >> $logfile
     $bcp dbo.arb_sray_262 in $toolcsv -S $DB_SERVER -d $DB_NAME -U $DB_USER -P $DB_PSW -b 5000 -m 999999 -F2 -q -c -t "|" -e $error >> $logs/tmp_sray_262_update.log
     $grepcmd "rows copied." $logs/tmp_sray_262_update.log >> $logfile
     if [ -s $error ];then
          body="Encounter error during bcp sray_262 on file: ${f}"
          echo -e "Subject: ${subject}\n${body}" | $sendmailcmd -t "${to}"
     fi
     #hookup to neo4j process
     $sqldel="DELETE a FROM dbo.neo4j_arb_sray_262 a WHERE NOT EXISTS (SELECT 1 FROM dbo.Companys b WHERE b.ticker_symbol = a.ticker);"
     $sqlcmd -S $DB_SERVER -d $DB_NAME -U $DB_USER -P $DB_PSW -I -Q "truncate table neo4j_arb_sray_262;"
     $bcp dbo.neo4j_arb_sray_262 in $toolcsv -S $DB_SERVER -d $DB_NAME -U $DB_USER -P $DB_PSW -b 5000 -m 999999 -F2 -q -c -t "|" -e $error
     $sqlcmd -S $DB_SERVER -d $DB_NAME -U $DB_USER -P $DB_PSW -I -Q "$sqldel"
     $bcp "select fsymid,ticker,date,esg,esg_e,esg_s,esg_g from neo4j_arb_sray_262" QUERYOUT $importfile -c -S $DB_SERVER -d $DB_NAME -U $DB_USER -P $DB_PSW -t"|"
     $awkcmd -i inplace 'BEGINFILE{print "fsymid|ticker|date|esg|esg_e|esg_s|esg_g"}{print}' $importfile
     $pythoncmd $home/bin/neo4j_update.py > $neo4j_error
     ret=$?
     if [ $ret -ne 0 ]; then
          body="Encountered error in ${partnet_name} Neo4j update\n $(head -5 $neo4j_error)"
          echo -e "Subject: ${subject}\n${body}" | $sendmailcmd -t "${to}"

     fi
  fi
  if [[ "$f" == *"sray_features_262"* ]];then
     echo "Processing $f file..." >> $logfile
     sqltext="delete from dbo.arb_sray_features_262 where date < '$(TZ=EST date -d "$DB_DATA_RETENTION" +%Y-%m-%d)';"
     $sqlcmd -S $DB_SERVER -d $DB_NAME -U $DB_USER -P $DB_PSW -I -Q "$sqltext" > $logs/tmp_sray_features_262.log
     $catcmd  $logs/tmp_sray_features_262.log >> $logfile
     $bcp dbo.arb_sray_features_262 in $toolcsv -S $DB_SERVER -d $DB_NAME -U $DB_USER -P $DB_PSW -b 1000 -m 999999 -F2 -q -c -t "|" -e $error >> $logs/tmp_sray_features_262.log
     $grepcmd "rows copied." $logs/tmp_sray_features_262.log >> $logfile
     if [ -s $error ];then
          body="Encounter error during bcp sray_features_262 on file: ${f}"
          echo -e "Subject: ${subject}\n${body}" | $sendmailcmd -t "${to}"
     fi

  fi
  if [[ "$f" == *"sray_preferences_300"* ]];then
     echo "Processing $f file..." >> $logfile
     sqltext="delete from dbo.arb_sray_preferences_300 where date < '$(TZ=EST date -d "$DB_DATA_RETENTION" +%Y-%m-%d)';"
     $sqlcmd -S $DB_SERVER -d $DB_NAME -U $DB_USER -P $DB_PSW -I -Q "$sqltext" > $logs/tmp_sray_preferences_300.log
     $catcmd  $logs/tmp_sray_preferences_300.log >> $logfile
     $bcp dbo.arb_sray_preferences_300 in $toolcsv -S $DB_SERVER -d $DB_NAME -U $DB_USER -P $DB_PSW -b 1000 -m 999999 -F2 -q -c -t "|" -e $error  >> $logs/tmp_sray_preferences_300.log
     $grepcmd "rows copied." $logs/tmp_sray_preferences_300.log >> $logfile
     if [ -s $error ];then
          body="Encounter error during bcp sray_pref_300 on file: ${f}"
          echo -e "Subject: ${subject}\n${body}" | $sendmailcmd -t "${to}"
     fi

  fi
  touch $files/$(basename "$f")
done

body="${partner_name} Data Feed is completed"
subject="${partner_name} Notification (Please do not reply to this email)"
echo -e "Subject: ${subject}\n${body}" | $sendmailcmd -t "${to}"

echo "End at: `date +%Y%m%d_%H%M%S`" >> $logfile
echo "" >> $logfile

exit 0

