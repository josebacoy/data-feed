#!/bin/bash

home="/home/SSAdmin/news"
tmp=$home/tmp
logs=$home/logs
files=$home/files
config=$home/config

DB_SERVER=$(grep db_server $config/app.conf | cut -d '=' -f 2-)
DB_NAME=$(grep db_name $config/app.conf | cut -d '=' -f 2-)
DB_USER=$(grep db_user $config/app.conf | cut -d '=' -f 2-)
DB_PSW=$(grep db_psw $config/app.conf | cut -d '=' -f 2-)
DB_DATA_RETENTION=$(grep db_data_retention $config/app.conf | cut -d '=' -f 2-)

subject=$(grep subject $config/app.conf | cut -d '=' -f 2-)
partner_name=$(grep partner $config/app.conf | cut -d '=' -f 2- | awk -F \" '{print $1}')
body=$(grep body $config/app.conf | cut -d '=' -f 2-)
to=$(grep to_mail $config/app.conf | cut -d '=' -f 2-)

tmpcsv=$tmp/*.csv
sendmailcmd="/usr/sbin/sendmail"
bcp="/opt/mssql-tools/bin/bcp"
sqlcmd="/opt/mssql-tools/bin/sqlcmd"
grepcmd="/bin/grep"
rmcmd="/bin/rm"
csvtoolcmd="/usr/bin/csvtool"
python3="/usr/bin/python3.8"

logfile=$logs/`date +"%Y%m%d_%H%M%S"`.log
echo "Start at: `date +%Y%m%d_%H%M%S`" >> $logfile

$python3 $home/bin/run_news.py
ret=$?
if [ $ret -ne 0 ]; then
    body="${partner_name} Feeds Alert Notification Error"
    subject="${partner_name} Error Notification (Please do not reply to this email)"
    echo -e "Subject: ${subject}\n${body}" | $sendmailcmd -t "${to}"
    exit 0
fi

cd $tmp
for f in $tmpcsv
do
  if [[ "$f" == *"out.csv"* ]];then 
     echo "Processing $f file..." >> $logfile
     sqltext="delete from dbo.news_feeds where newsPublishedOn < '$(TZ=EST date -d "$DB_DATA_RETENTION" +%Y-%m-%d)';"
     $sqlcmd -S $DB_SERVER -d $DB_NAME -U $DB_USER -P $DB_PSW -I -Q "$sqltext" > $logs/tmp_news_feeds.log
     cat  $logs/tmp_news_feeds.log >> $logfile
     $bcp dbo.news_feeds in $f -S $DB_SERVER -d $DB_NAME -U $DB_USER -P $DB_PSW -b 5000 -m 999999 -F2 -q -c -t "|" -e $logs/news_feeds.error >> $logs/tmp_news_feeds.log
     $grepcmd "rows copied." $logs/tmp_news_feeds.log >> $logfile
  fi
  touch $tmp/$(basename "$f")
done

body="${partner_name} Data Feed is success"
subject="${partner_name} Notification (Please do not reply to this email)"
echo -e "Subject: ${subject}\n${body}" | $sendmailcmd -t "${to}"

echo "End at: `date +%Y%m%d_%H%M%S`" >> $logfile
echo "" >> $logfile

exit 0

